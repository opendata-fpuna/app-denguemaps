package com.app.domain;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.app.utils.tables.annotation.AttributeDescriptor;

@Entity
public class Reporte implements Serializable {

	private static final long serialVersionUID = 1L;

	@AttributeDescriptor(path = "id")
	@Id
	@SequenceGenerator(name = "reporte_id_seq", schema = "public", sequenceName = "reporte_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "reporte_id_seq")
	@Column
	private int id;

	@AttributeDescriptor(path = "cantidad")
	@Column
	private Long cantidad;

	@AttributeDescriptor(path = "semana")
	@Column
	private String semana;

	@AttributeDescriptor(path = "anio")
	@Column
	private String anio;

	@AttributeDescriptor(path = "mes")
	@Column
	private Integer mes;

	@AttributeDescriptor(path = "dia")
	@Column
	private Integer dia;

	@AttributeDescriptor(path = "region")
	@Column
	private String region;

	@AttributeDescriptor(path = "pais")
	@Column
	private String pais;

	@AttributeDescriptor(path = "adm1")
	@Column
	private String adm1;

	@AttributeDescriptor(path = "adm2")
	@Column
	private String adm2;

	@AttributeDescriptor(path = "adm3")
	@Column
	private String adm3;

	@AttributeDescriptor(path = "grupo_edad")
	@Column
	private String grupo_edad;

	@AttributeDescriptor(path = "sexo")
	@Column
	private String sexo;

	@AttributeDescriptor(path = "clasificacion_clinica")
	@Column
	private String clasificacion_clinica;

	@AttributeDescriptor(path = "estado_final")
	@Column
	private String estado_final;

	@AttributeDescriptor(path = "serotipo")
	@Column
	private String serotipo;

	@AttributeDescriptor(path = "origen")
	@Column
	private String origen;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSemana() {
		return semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getDia() {
		return dia;
	}

	public void setDia(Integer dia) {
		this.dia = dia;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getAdm1() {
		return adm1;
	}

	public void setAdm1(String adm1) {
		this.adm1 = adm1;
	}

	public String getAdm2() {
		return adm2;
	}

	public void setAdm2(String adm2) {
		this.adm2 = adm2;
	}

	public String getAdm3() {
		return adm3;
	}

	public void setAdm3(String adm3) {
		this.adm3 = adm3;
	}

	public String getGrupo_edad() {
		return grupo_edad;
	}

	public void setGrupo_edad(String grupo_edad) {
		this.grupo_edad = grupo_edad;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getClasificacion_clinica() {
		return clasificacion_clinica;
	}

	public void setClasificacion_clinica(String clasificacion_clinica) {
		this.clasificacion_clinica = clasificacion_clinica;
	}

	public String getEstado_final() {
		return estado_final;
	}

	public void setEstado_final(String estado_final) {
		this.estado_final = estado_final;
	}

	public String getSerotipo() {
		return serotipo;
	}

	public void setSerotipo(String serotipo) {
		this.serotipo = serotipo;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

}
