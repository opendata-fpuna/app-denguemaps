package com.app.tables.dao;

import java.util.List;

import com.app.domain.Reporte;
import com.app.utils.tables.dao.GenericDaoImpl;
import com.app.utils.tables.filter.BaseFilter;

/**
 * Created by rparra on 25/3/15.
 * Estas implementaciones son de prueba, hasta implementar el DAO generico
 */
public class ReporteTableDAO extends GenericDaoImpl<Reporte> {

    @Override
    public List<Reporte> getEntities(List<String> attributes, List<List<BaseFilter<?>>> filters, Integer pageSize, Integer offset) {
        return super.getEntities(attributes, filters, pageSize, offset);
    }

    @Override
    public Integer getEntitiesCount() {
        return super.getEntitiesCount();
    }

    @Override
    public Integer getFilteredEntitiesCount(List<String> attributes, List<List<BaseFilter<?>>> filters) {
        return super.getFilteredEntitiesCount(attributes, filters);
    }
}

