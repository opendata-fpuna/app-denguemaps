package com.app.tables.facade;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.app.domain.Reporte;
import com.app.tables.dao.ReporteTableDAO;
import com.app.utils.tables.facade.DatatablesFacadeImpl;

public class ReporteTableFacade extends DatatablesFacadeImpl<Reporte> {
	
	@Inject
	ReporteTableDAO reporteDao;
	
	@PostConstruct
    public void init(){
		System.out.println("ReporteTableFacade --> init()");
        if(this.dao == null){
            this.dao = reporteDao;
        }
    }
}
