package com.app.tables.facade;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.app.domain.Notificacion;
import com.app.tables.dao.NotificacionTableDAO;
import com.app.utils.tables.facade.DatatablesFacadeImpl;

public class NotificacionTableFacade extends DatatablesFacadeImpl<Notificacion> {
	
	@Inject
	NotificacionTableDAO notificacionDao;
	
	@PostConstruct
    public void init(){
		System.out.println("NotificacionTableFacade --> init()");
        if(this.dao == null){
            this.dao = notificacionDao;
        }
    }
}
