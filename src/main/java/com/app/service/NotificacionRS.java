package com.app.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.app.constant.Statics;
import com.app.domain.Notificacion;
import com.app.persistence.NotificacionDAO;
import com.app.tables.facade.NotificacionTableFacade;
import com.app.utils.NativeQueryFileManager;

@Named
@RequestScoped
@Path("/rest/notificacion")
public class NotificacionRS {

	private final static Logger logger = Logger.getLogger(NotificacionRS.class
			.getName());

	@Inject
	NotificacionDAO notificacionDao;

	@Inject
	NotificacionTableFacade facade;

	@Inject
	private NativeQueryFileManager n;

	@GET
	@Path("/lista")
	@Produces("application/json")
	public Response getNotificaciones(@Context UriInfo uriInfo) {
		MultivaluedMap<String, String> parameters = uriInfo
				.getQueryParameters();
		Map<String, String[]> finalMap = new HashMap<String, String[]>();
		Set<String> keys = parameters.keySet();
		for (String key : keys) {
			List<String> list = parameters.get(key);
			String[] a = new String[] {};
			finalMap.put(key, (String[]) list.toArray(a));
		}
		return Response.status(200).entity(facade.getResult(finalMap)).build();
	}

	@GET
	@Path("/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonNotificacionesPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de notificaciones por año");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.NOTIFPORANIO, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GET
	@Path("/riesgo/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonRiesgosPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de riesgos por departamento");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.RIESGOSPORANIO, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GET
	@Path("/riesgo/distrito/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonRiesgosDistritoPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de riesgos por distrito");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.REISGOSDITRITOS, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GET
	@Path("/riesgo/asuncion/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonRiesgosAsuncionPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de riesgos por barrios de Asunción");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.RIESGOSASU, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GET
	@Path("/filtros")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNotificacionesFiltradas(@QueryParam("anio") String anio,
			@QueryParam("semana") String semana,
			@QueryParam("fechaNotificacion") String fechaNotificacion,
			@QueryParam("departamento") String departamento,
			@QueryParam("distrito") String distrito,
			@QueryParam("sexo") String sexo, @QueryParam("edad") String edad,
			@QueryParam("resultado") String resultado) {
		logger.info("+++++++++++++++++ Servicio de notificaciones filtradas");
		List<Notificacion> list = notificacionDao.getNotificacionesFiltradas(
				anio, semana, fechaNotificacion, departamento, distrito, sexo,
				edad, resultado);

		return Response.ok(list).build();
	}

	@GET
	@Path("/filtrosmapa")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonNotificacionesPorFiltros(
			@QueryParam("anio") String anio, @QueryParam("f") int f,
			@QueryParam("m") int m, @QueryParam("confirmado") int confirmado,
			@QueryParam("descartado") int descartado,
			@QueryParam("sospechoso") int sospechoso) {
		String query = "";
		if (f == 1) {
			query = query + " and sexo = 'F'";
		}
		if (m == 1) {
			query = query + " and sexo = 'M'";
		}
		if (confirmado == 1 || descartado == 1 || sospechoso == 1) {
			query = query + " and (";
			if (confirmado == 1) {
				query = query + " clasificacon_clinica = 'CONFIRMADO'";
			}
			if (descartado == 1) {
				if (confirmado == 1) {
					query = query + " or ";
				}
				query = query + "clasificacon_clinica = 'DESCARTADO'";
			}
			if (sospechoso == 1) {
				if (descartado == 1 || confirmado == 1) {
					query = query + " or ";
				}
				query = query + "clasificacon_clinica = 'SOSPECHOSO'";
			}
			query = query + " ) ";
		}
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.NOTIFFILTRADASMAP,
					anio, query);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}
}
