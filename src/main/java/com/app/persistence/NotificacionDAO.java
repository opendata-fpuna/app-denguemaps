package com.app.persistence;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;

import com.app.domain.Notificacion;

@Stateless
public class NotificacionDAO {

	@Inject
	EntityManager em;

	public List<Notificacion> getNotificaciones() {

		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(Notificacion.class);

		return criteria.list();

	}
	
	public List<Notificacion> getNotificacionesPorAnio(String anio) {

		Query q = em.createQuery("select n from Notificacion n where n.anio='"
				+ anio + "'");
		try {
			List<Notificacion> listaevento = (List<Notificacion>) q
					.getResultList();
			return listaevento;
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Notificacion> getNotificacionesFiltradas(String anio,
			String semana, String fechaNotificacion, String departamento,
			String distrito, String sexo, String edad, String resultado) {

		String wheres = "";

		if (anio != null && anio.compareTo("") != 0) {
			wheres = wheres + " n.anio='" + anio + "'";
		}
		if (semana != null && semana.compareTo("") != 0) {
			if (wheres.compareTo("") != 0) {
				wheres = wheres + " and " + " n.semana like '%" + semana + "%'";
			} else {
				wheres = wheres + " n.semana like '%" + semana + "%'";
			}
		}
		if (fechaNotificacion != null && fechaNotificacion.compareTo("") != 0) {
			if (wheres.compareTo("") != 0) {
				wheres = wheres + " and " + " n.fecha_notificacion like '%"
						+ fechaNotificacion + "%'";
			} else {
				wheres = wheres + " n.fecha_notificacion like '%" + fechaNotificacion
						+ "%'";
			}
		}
		if (departamento != null && departamento.compareTo("") != 0) {
			if (wheres.compareTo("") != 0) {
				wheres = wheres + " and " + " n.departamento like '%" + departamento.toUpperCase()
						+ "%'";
			} else {
				wheres = wheres + " n.departamento like '%" + departamento.toUpperCase() + "%'";
			}
		}
		if (distrito != null && distrito.compareTo("") != 0) {
			if (wheres.compareTo("") != 0) {
				wheres = wheres + " and " + " n.distrito like '%" + distrito.toUpperCase() + "%'";
			} else {
				wheres = wheres + " n.distrito like '%" + distrito.toUpperCase() + "%'";
			}
		}
		if (sexo != null && sexo.compareTo("") != 0) {
			if (wheres.compareTo("") != 0) {
				wheres = wheres + " and " + " n.sexo like '%" + sexo.toUpperCase() + "%'";
			} else {
				wheres = wheres + " n.sexo like '%" + sexo.toUpperCase() + "%'";
			}
		}

		if (edad != null && edad.compareTo("") != 0) {
			if (wheres.compareTo("") != 0) {
				wheres = wheres + " and " + " n.edad like '%" + edad.toUpperCase() + "%'";
			} else {
				wheres = wheres + " n.edad like '%" + edad.toUpperCase() + "%'";
			}
		}

		if (resultado != null && resultado.compareTo("") != 0) {
			if (wheres.compareTo("") != 0) {
				wheres = wheres + " and " + " n.clasificacon_clinica like '%"
						+ resultado.toUpperCase() + "%'";
			} else {
				wheres = wheres + " n.clasificacon_clinica like '%" + resultado.toUpperCase() + "%'";
			}
		}
		String query = "select n from Notificacion n";
		if (wheres.compareTo("") != 0) {
			query = query + " where " + wheres;
		} else {
			return null;
		}
		System.out.println("query " + query);
		Query q = em.createQuery(query);
		try {
			List<Notificacion> listaevento = (List<Notificacion>) q
					.getResultList();
			return listaevento;
		} catch (NoResultException e) {
			return null;
		}
	}
}
