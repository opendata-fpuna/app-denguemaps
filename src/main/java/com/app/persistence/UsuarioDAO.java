package com.app.persistence;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.app.domain.Usuario;

@Stateless
public class UsuarioDAO {

	@Inject
	EntityManager em;

	public Usuario getUsuario(String username, String pwd) {

		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(Usuario.class);
		criteria.add(Restrictions.like("username", username));
		criteria.add(Restrictions.like("pwd", pwd));

		if (criteria.list().size() == 1) {
			return (Usuario) criteria.list().get(0);
		} else {
			return null;
		}

	}

}
