package com.app.constant;

/*
 * Esta clase cuenta con todas las constantes del sistema.
 * 
 */
public class Statics {

	/** QUERYS **/
	public final static String RIESGOSASU = "select * from riesgosAsuncion where anio = '{0}'"; 
	
	public final static String REISGOSDITRITOS = "select * from riesgosDistritos where anio = '{0}'" ;
	
	public final static String RIESGOSPORANIO = "select * from riesgosDepartamentos where anio = '{0}'";
	
	public final static String NOTIFFILTRADASMAP = "select count(*) as cantidad, departamento, semana, anio from notificacion where anio = '{0}' {1} group by anio, semana, departamento order by semana";

	public final static String NOTIFPORANIO = "select noti.departamento, CAST(coalesce(noti.semana, '0') AS integer) as semana, count(*) as cantidad from notificacion noti where anio='{0}' and semana not like '#VALUE!' group by departamento, semana order by CAST(coalesce(noti.semana, '0') AS integer), departamento";
	
	public final static String REPORPORANIO = "select * from reporte r where anio='{0}'";

}
