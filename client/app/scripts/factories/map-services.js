'use strict';



angular.module('denguemapsApp')
  .factory('mapServices', function($http, $q) {
  	return {
  		getRiesgosDepartamentos: getRiesgosDepartamentos,
      getRiesgosDistritos: getRiesgosDistritos,
      getRiesgosAsuncion: getRiesgosAsuncion,
  		getDepartamentos: getDepartamentos,
      getDistritos: getDistritos,
      getBarrios: getBarrios,
      getCasosDepartamentos: getCasosDepartamentos
  	};

  	function getRiesgosDepartamentos(anio){
  		var defered = $q.defer();
  		var promise = defered.promise;
      var url = 'http://localhost:8088/denguemaps-server/rest/notificacion/riesgo/' + anio;
  		$http.get(url, {cache: true})
  			.success(function(data){
  				defered.resolve(data);
  			})
  			.error(function(err){
  				defered.reject(err);
  			});
  		return promise;
  	}

    function getRiesgosDistritos(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = 'http://localhost:8088/denguemaps-server/rest/notificacion/riesgo/distrito/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getRiesgosAsuncion(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = 'http://localhost:8088/denguemaps-server/rest/notificacion/riesgo/asuncion/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    /* EN LA OFICINA AGREGARLE EL NRO 2 */
  	function getDepartamentos(){
  		var defered = $q.defer();
  		var promise = defered.promise;
		  //var url = 'http://192.168.1.106:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_departamento_paraguay&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      var url = 'http://localhost:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_departamento_paraguay_2&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
  		$http.jsonp(url, {cache: true})
  			.success(function(data){
  				defered.resolve(data);
  			})
  			.error(function(err){
  				defered.reject(err);
  			});
  		return promise;
  	}

    /* ESTOS DOS TDV NO SE ESTAN USANDO, FALTA PROBAR */
    function getDistritos(dpto){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = 'http://192.168.1.106:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_distrito_paraguay&CQL_FILTER=first_dp_1=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      //var url = 'http://localhost:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_distrito_paraguay&CQL_FILTER=first_dp_1=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      $http.jsonp(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getBarrios(dpto){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = 'http://192.168.1.106:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_barrio_localidad_paraguay&CQL_FILTER=dpto_desc=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      //var url = 'http://localhost:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_barrio_localidad_paraguay&CQL_FILTER=dpto_desc=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      $http.jsonp(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getCasosDepartamentos(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = 'http://localhost:8088/denguemaps-server/rest/notificacion/filtrosmapa?anio=' + anio +'&confirmado='+1+'&descartado='+1+'&sospechoso='+1+'&f=0&m=0';
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

});