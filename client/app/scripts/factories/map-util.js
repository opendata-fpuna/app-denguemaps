'use strict';



angular.module('denguemapsApp')
  .factory('mapUtil', function(mapServices, $q) {
  		return {
  			getInfo: getInfo,
  		};

  		function getInfo(){
			info = L.control();
		    info.onAdd = function (map) {
		        this._div = L.DomUtil.create('div', 'info');
		        this.update();
		        return this._div;
		    };
		    info.update = function (props) {

		        if(props){

		            var dep = props.dpto_desc;
		            //if(SMV.onriesgo){
		                var mapSem = riesgoSemanaDep;
		            /*}else{
		                var mapSem = SMV.mapSemFil;
		            }*/

		            var nroNS = '0';
		            try{
		                nroNS = mapSem[dep]["cantidad"];
		            }catch(e){

		            }
		          this._div.innerHTML =  '<h2>Año: '+anio+' - Semana: '+semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
		        }
		    };
		    info.updateDrillDown = function (props){

		        if(props){

		            var dep = props.first_dp_1;
		            var depAsu = props.dpto_desc;
		            var dis = props.first_di_1;
		            var mapSem = riesgoSemanaDis;
		            var nroNS = '0';

		            var info;
		            if(depAsu == 'ASUNCION'){
		            	dis = props.dist_desc;
		                var key = depAsu+'-'+props.barlo_desc;
		                try{
		                    nroNS = mapSem[key]["cantidad"];
		                }catch(e){

		                }
		                info = '<h2>Año: '+anio+' - Semana: '+semana+'<\/h2><h2>Dpto: '+depAsu+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Barrio: '+props.barlo_desc+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
		            } else {
		                var key = dep+'-'+dis;
		                try{
		                    nroNS = mapSem[key]["cantidad"];
		                }catch(e){

		                }
		                info = '<h2>Año: '+anio+' - Semana: '+semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
		            }
		          this._div.innerHTML =  info;
		        }
	    	};
	    	return info;
	    }

});